# Formulário

## Enunciado

O programa deve solicitar um número ao usuário, e dizer se o número é par ou ímpar.

## Resultado esperado

A tela de iteração do programa deve ter o formato abaixo:

```
$ python3 main.py
Informe um número: 12

O número informado é par
```

## Pontos importantes

- Preste atenção no espaçamento e acentuação para que o texto saia conforme o exemplo acima
- Não esqueça da converter o valor recebido para número, quando necessário
- O que acontece com o seu programa se o usuário inserir algo além de um número?

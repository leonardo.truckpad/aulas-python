# Poema

## Enunciado

Neste exercício, você deve criar um programa que imprima na tela o poema "Poeminha do contra", de Mario Quintana:

> Todos esses que aí estão
> Atravancando meu caminho,
> Eles passarão...
> Eu passarinho!

## Resultado esperado

A saida do programa deve ter o formato abaixo:

```
$ python3 main.py
Poeminha do contra

Todos esses que aí estão
Atravancando meu caminho,
Eles passarão...
Eu passarinho!

- Mario Quintana
```

## Pontos importantes

- Preste atenção no espaçamento e acentuação para que o texto saia conforme o exemplo acima
- Há várias formas de exibir um texto na tela, utilize a forma que achar mais conveniente

# Papagaio

## Enunciado

Um papagaio sempre repete coisas que a gente fala pra ele. Neste exercício, crie um programa que exiba na tela exatamente aquilo que o usuário escreveu.

## Resultado esperado

A tela de iteração do programa deve ter o formato abaixo:

```
$ python3 main.py
Diga algo ao papagaio: paralelepipedo

Papagaio: paralelepipedo
```

O texto `paralelepipedo` é um exemplo de texto digitado pelo usuário, no código final ele deve ter a possibilidade de escrever qualquer texto

## Pontos importantes

- Preste atenção no espaçamento e acentuação para que o texto saia conforme o exemplo acima
- Use a forma que preferir para concatenar os textos

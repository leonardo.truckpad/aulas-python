# Formulário

## Enunciado

Vamos criar um formulário simples com 3 perguntas, e com as respostas gerar uma frase descrevendo o usuário

Devem ser feitas 3 perguntas:
- Nome
- Ano de nascimento
- Local onde mora

Após receber as perguntas, deve ser gerada a seguinte frase:

> [Nome], [Idade], é morador de [Cidade]

## Resultado esperado

A tela de iteração do programa deve ter o formato abaixo:

```
$ python3 main.py
Informe seu nome: Leonardo
Informe seu ano de nascimento: 1985
Informe seu local de residência: São Paulo

Leonardo, 38, é morador de São Paulo
```

## Pontos importantes

- Preste atenção no espaçamento e acentuação para que o texto saia conforme o exemplo acima
- Não esqueça da converter o texto para número, quando necessário
- Formate o texto final da forma que achar necessário

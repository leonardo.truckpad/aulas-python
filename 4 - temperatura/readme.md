# Formulário

## Enunciado

Vamos criar um programa que receba uma temperatura em grus Célsius (°C) e o converta para Farenheight (°F)

A formula para converter os valores é:
 
F = (C * 1,8) + 32

## Resultado esperado

A tela de iteração do programa deve ter o formato abaixo:

```
$ python3 main.py
Informe a temperatura (°C): 35

35°C é o equivalente a 95°F
```

## Pontos importantes

- Preste atenção no espaçamento e acentuação para que o texto saia conforme o exemplo acima
- Não esqueça da converter o texto para número, quando necessário
